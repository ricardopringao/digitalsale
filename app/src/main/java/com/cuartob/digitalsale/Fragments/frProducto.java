package com.cuartob.digitalsale.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cuartob.digitalsale.Objetos.Publicacion;
import com.cuartob.digitalsale.Objetos.Usuario;
import com.cuartob.digitalsale.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class frProducto extends Fragment {

    public frProducto() {
        // Required empty public constructor
    }

    TextView tvTitulo, tvAutor, tvDescripcion, tvUbicacion, tvPrecio;
    ImageView fotoProducto;
    Button btnComprar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_producto, container, false);

        tvTitulo = (TextView) view.findViewById(R.id.tvTituloProducto);
        tvAutor = (TextView) view.findViewById(R.id.tvAutorProducto);
        tvDescripcion = (TextView) view.findViewById(R.id.tvDescripcionProducto);
        tvUbicacion = (TextView) view.findViewById(R.id.tvUbicacionProducto);
        tvPrecio = (TextView) view.findViewById(R.id.tvPrecioProducto);
        fotoProducto = (ImageView) view.findViewById(R.id.ivfotoProducto);
        btnComprar = (Button) view.findViewById(R.id.btnComprar);
        verPublicacion();
        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprar();
            }
        });
        return view;
    }

    private void verPublicacion() {
        Bundle bundle = getArguments();
        String titulo = bundle.getString("titulo");
        String descripcion = bundle.getString("descripcion");
        String autor = bundle.getString("autor");
        String urlFoto = bundle.getString("foto");
        String ubicacion = bundle.getString("ubicacion");
        String precio = bundle.getString("precio");

        tvTitulo.setText("Título: " + titulo);
        tvAutor.setText("Autor: " + autor);
        tvDescripcion.setText("Descripción: " + descripcion);
        tvPrecio.setText("Precio: $" + precio);
        tvUbicacion.setText("Ubicación: " + ubicacion);
        Picasso.get().load(urlFoto).into(fotoProducto);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String usuario = mAuth.getCurrentUser().getEmail();

        if (autor.equals(usuario)) {
            btnComprar.setVisibility(View.INVISIBLE);
        }else {
            btnComprar.setVisibility(View.VISIBLE);
        }
    }

    private void comprar() {
        Bundle bundle = getArguments();
        String autor = bundle.getString("autor");
        DatabaseReference db = FirebaseDatabase.getInstance().getReference("Usuario");
        Query q = db.orderByChild(getString(R.string.correo)).equalTo(autor);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Usuario user = snapshot.getValue(Usuario.class);
                        String dial = "tel:" + user.getTelefono();
                        Toast.makeText(getContext(), "Contacte con el vendedor", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
                    }
                }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
