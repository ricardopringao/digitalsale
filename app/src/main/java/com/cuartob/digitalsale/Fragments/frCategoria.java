package com.cuartob.digitalsale.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cuartob.digitalsale.Adaptador.AdaptadorCategoria;
import com.cuartob.digitalsale.Activitys.MenuCategoria;
import com.cuartob.digitalsale.R;

import java.util.ArrayList;


public class frCategoria extends Fragment {

    private ListView listView;
    private AdaptadorCategoria adaptadorCategoria;


    public frCategoria() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_fr_categoria, container, false);
        listView = (ListView) vista.findViewById(R.id.lvItemCategoria);
        adaptadorCategoria = new AdaptadorCategoria(getContext(), getArrayItems());
        listView.setAdapter(adaptadorCategoria);
        return vista;

    }

    private ArrayList<MenuCategoria> getArrayItems(){
        final ArrayList<MenuCategoria> listItems = new ArrayList<>();
        listItems.add(new MenuCategoria("Pinturas"));
        listItems.add(new MenuCategoria("Esculturas"));
        listItems.add(new MenuCategoria("Artesanias"));
        listItems.add(new MenuCategoria("Antiguedades"));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment miFragment = new frArticulosCategoria();
                Bundle bundle = new Bundle();
                String item = listItems.get(position).getItemCategoria();
                bundle.putString("item", item);
                miFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_main ,miFragment).addToBackStack(null).commit();
            }
        });

        return listItems;
    }



}
