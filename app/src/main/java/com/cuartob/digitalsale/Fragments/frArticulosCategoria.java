package com.cuartob.digitalsale.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cuartob.digitalsale.Adaptador.AdaptadorArtworks;
import com.cuartob.digitalsale.Adaptador.AdaptadorItemsCategoria;
import com.cuartob.digitalsale.Objetos.Publicacion;
import com.cuartob.digitalsale.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class frArticulosCategoria extends Fragment {


    public frArticulosCategoria() {
        // Required empty public constructor
    }

    RecyclerView rv;
    List<Publicacion> lista;
    AdaptadorItemsCategoria adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_fr_articulos_categoria, container, false);
        TextView lblNombreCategoria = (TextView) vista.findViewById(R.id.lblNombreCategoria);
        Bundle bundle = getArguments();
        String nombreCategoria = bundle.getString("item");
        lblNombreCategoria.setText(nombreCategoria);
        rv = (RecyclerView) vista.findViewById(R.id.recyclerCategoria);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        lista = new ArrayList<>();
        adapter = new AdaptadorItemsCategoria(lista);
        rv.setAdapter(adapter);
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        Query q = db.getReference("Publicacion").orderByChild(getString(R.string.tipo)).equalTo(nombreCategoria);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Publicacion post = snapshot.getValue(Publicacion.class);
                    lista.add(post);
                }adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment miFragment = new frProducto();
                Bundle bundle = new Bundle();
                bundle.putString("foto", lista.get(rv.getChildAdapterPosition(v)).getUrl());
                bundle.putString("titulo", lista.get(rv.getChildAdapterPosition(v)).getTitulo());
                bundle.putString("descripcion", lista.get(rv.getChildAdapterPosition(v)).getDescripcion());
                bundle.putString("precio", lista.get(rv.getChildAdapterPosition(v)).getPrecio());
                bundle.putString("ubicacion", lista.get(rv.getChildAdapterPosition(v)).getUbicacion());
                bundle.putString("autor", lista.get(rv.getChildAdapterPosition(v)).getAutor());
                miFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_main ,miFragment).addToBackStack(null).commit();
            }
        });
        return vista;
    }
}