package com.cuartob.digitalsale.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cuartob.digitalsale.Objetos.Usuario;
import com.cuartob.digitalsale.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class frMiperfil extends Fragment {

    public frMiperfil() {
        // Required empty public constructor
    }


    TextView lblNombre, lblDireccion, lblCorreo, lblTelefono, lblCedula;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fr_miperfil, container, false);
        lblNombre = (TextView) view.findViewById(R.id.lblnombre);
        lblDireccion = (TextView) view.findViewById(R.id.lbldireccion);
        lblCorreo = (TextView) view.findViewById(R.id.lblcorreo);
        lblTelefono = (TextView) view.findViewById(R.id.lbltelefono);
        lblCedula = (TextView) view.findViewById(R.id.lblcedula);

        miPerfil();

        // Inflate the layout for this fragment
        return view;
    }

    private void miPerfil() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()){
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    String key = myRef.child(mAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)) {
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        lblNombre.setText("Nombre: " + usuario_perfil.getNombre());
                        lblCorreo.setText("Correo: " + usuario_perfil.getCorreo());
                        lblDireccion.setText("Direccion: " + usuario_perfil.getDireccion());
                        lblTelefono.setText("Telefono: " +usuario_perfil.getTelefono());
                        lblCedula.setText("Cedula: " + usuario_perfil.getCedula());
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //Mensaje de Error
            }
        });
    }

}
