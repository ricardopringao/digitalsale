package com.cuartob.digitalsale.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.cuartob.digitalsale.Activitys.PrincipalActivity;
import com.cuartob.digitalsale.Objetos.Publicacion;
import com.cuartob.digitalsale.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


public class Publicarproducto extends Fragment {


    TextView txtTitulo, txtUbicacion, txtDescripcion, txtPrecio;
    private FirebaseAuth mAuth;
    RadioButton rbpinturas, rbesculturas, rbartesanias, rbantiguedades;
    Button btnEnviar, btnFoto;
    ImageView ivEnviarFoto;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Uri uri;
    private String tipo, uuid;

    public Publicarproducto() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_publicarproducto, container, false);
        txtTitulo = (TextView) vista.findViewById(R.id.txtTitulo);
        txtUbicacion = (TextView) vista.findViewById(R.id.txtUbicacion);
        txtDescripcion = (TextView) vista.findViewById(R.id.txtDescripcion);
        txtPrecio = (TextView) vista.findViewById(R.id.txtPrecio);
        rbpinturas = (RadioButton) vista.findViewById(R.id.rbpintura);
        rbesculturas = (RadioButton) vista.findViewById(R.id.rbescultura);
        rbartesanias = (RadioButton) vista.findViewById(R.id.rbartesanias);
        rbantiguedades = (RadioButton) vista.findViewById(R.id.rbantiguedades);
        btnEnviar = (Button) vista.findViewById(R.id.btnEnviar);
        btnFoto = (Button) vista.findViewById(R.id.btnFoto);
        ivEnviarFoto = (ImageView) vista.findViewById(R.id.ivEnviarFoto);
        uri = null;
        uuid = UUID.randomUUID().toString();

        mAuth = FirebaseAuth.getInstance();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Publicacion");
        StorageReference storage = FirebaseStorage.getInstance().getReference();


        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent, "seleccionar imagen"), 10);
            }
        });


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publicarProducto();
            }
        });
        return vista;
    }


    private void publicarProducto() {
        if (rbpinturas.isChecked() == true) {
            tipo = rbpinturas.getText().toString();
        } else if (rbesculturas.isChecked() == true) {
            tipo  = rbesculturas.getText().toString();
        } else if (rbantiguedades.isChecked() == true) {
            tipo  = rbantiguedades.getText().toString();
        } else if (rbartesanias.isChecked() == true) {
            tipo  = rbartesanias.getText().toString();
        }

        StorageReference storage = FirebaseStorage.getInstance().getReference();
        final StorageReference file = storage.child("Publicacion").child(uri.getLastPathSegment());
        file.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                file.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Publicacion post = new Publicacion();
                        post.setTitulo(txtTitulo.getText().toString());
                        post.setUbicacion(txtUbicacion.getText().toString());
                        post.setDescripcion(txtDescripcion.getText().toString());
                        post.setTipo(tipo);
                        post.setPrecio(txtPrecio.getText().toString());
                        post.setUrl(uri.toString());
                        post.setAutor(mAuth.getCurrentUser().getEmail());
                        databaseReference.child(uuid).setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getContext(), "SE SUBIO", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getContext(), PrincipalActivity.class));
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            uri = data.getData();
            ivEnviarFoto.setImageURI(uri);
        }
    }
}