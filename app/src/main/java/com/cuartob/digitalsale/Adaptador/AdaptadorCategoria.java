package com.cuartob.digitalsale.Adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cuartob.digitalsale.Activitys.MenuCategoria;
import com.cuartob.digitalsale.R;

import java.util.ArrayList;

public class AdaptadorCategoria extends BaseAdapter {

    private Context context;
    private ArrayList<MenuCategoria> listCategoria;

    public AdaptadorCategoria(Context context, ArrayList<MenuCategoria> listCategoria) {
        this.context = context;
        this.listCategoria = listCategoria;
    }

    @Override
    public int getCount() {
        return listCategoria.size();
    }

    @Override
    public Object getItem(int position) {
        return listCategoria.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuCategoria item = (MenuCategoria) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.item_categoria, null);
        TextView tvItem = (TextView) convertView.findViewById(R.id.itemCategoria);

        tvItem.setText(item.getItemCategoria());

        return convertView;
    }
}
