package com.cuartob.digitalsale.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuartob.digitalsale.Objetos.Publicacion;
import com.cuartob.digitalsale.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdaptadorItemsCategoria
        extends RecyclerView.Adapter<AdaptadorItemsCategoria.ArtCatViewHolder>
        implements View.OnClickListener{

    private View.OnClickListener listener;
    List<Publicacion> listPublicacion;

    public AdaptadorItemsCategoria(List<Publicacion> listPublicacion) {
        this.listPublicacion = listPublicacion;
    }


    @Override
    public ArtCatViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        view.setOnClickListener(this);
        ArtCatViewHolder holder = new ArtCatViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ArtCatViewHolder holder, int position) {
        holder.tvTitulo.setText(listPublicacion.get(position).getTitulo());
        holder.tvPrecio.setText("$" + listPublicacion.get(position).getPrecio());
        holder.tvDescripcion.setText(listPublicacion.get(position).getDescripcion());
        Picasso.get().load(listPublicacion.get(position).getUrl()).into(holder.ivArte);
    }

    @Override
    public int getItemCount() {
        return listPublicacion.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public class ArtCatViewHolder extends RecyclerView.ViewHolder {

        ImageView ivArte;
        TextView tvTitulo, tvPrecio, tvDescripcion;

        public ArtCatViewHolder(View itemView) {
            super(itemView);
            ivArte = (ImageView) itemView.findViewById(R.id.ivPost);
            tvTitulo = (TextView) itemView.findViewById(R.id.textViewtitulo);
            tvPrecio = (TextView) itemView.findViewById(R.id.textViewprecio);
            tvDescripcion = (TextView) itemView.findViewById(R.id.textViewdescripcion);
        }
    }
}
