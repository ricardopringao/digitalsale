package com.cuartob.digitalsale.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.cuartob.digitalsale.Fragments.frAjustes;
import com.cuartob.digitalsale.Fragments.frCategoria;
import com.cuartob.digitalsale.Fragments.frInicio;
import com.cuartob.digitalsale.Fragments.frMiperfil;
import com.cuartob.digitalsale.Fragments.Publicarproducto;
import com.cuartob.digitalsale.Objetos.Usuario;
import com.cuartob.digitalsale.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PrincipalActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Fragment miFragment = new frInicio();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, miFragment).addToBackStack(null).commit();

        cargarInformacionUsuario();

    }

    private void cargarInformacionUsuario() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    TextView lbl_usuario = (TextView) findViewById(R.id.AndroidUsuario);
                    TextView  lbl_correo = (TextView) findViewById(R.id.AndroidCorreo);
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    String key = myRef.child(mAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)) {
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        Toast.makeText(getApplicationContext(),"Bienvenido: " + usuario_perfil.getNombre(), Toast.LENGTH_SHORT).show();
                        lbl_usuario.setText(usuario_perfil.getNombre());
                        lbl_correo.setText(usuario_perfil.getCorreo());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment miFragment = null;
        boolean fragmentSeleccionado = false;

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            miFragment = new frInicio();
            fragmentSeleccionado = true;
        }else if (id == R.id.nav_categoria) {
            miFragment = new frCategoria();
            fragmentSeleccionado = true;
        }else if (id == R.id.nav_perfil) {
            miFragment = new frMiperfil();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_publicar) {
            miFragment = new Publicarproducto();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_cerrar_sesion) {
            mAuth.signOut();
            Toast.makeText(getApplicationContext(), "Has Cerrado Sesión", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PrincipalActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_ajustes) {
            miFragment = new frAjustes();
            fragmentSeleccionado = true;
        }/* else if (id == R.id.nav_send) {

        } */

        if (fragmentSeleccionado == true){
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main ,miFragment).addToBackStack(null).commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
