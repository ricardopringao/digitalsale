package com.cuartob.digitalsale.Activitys;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cuartob.digitalsale.Objetos.Usuario;
import com.cuartob.digitalsale.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class RegistroActivity extends AppCompatActivity {

    Button btnRegistrar;
    private FirebaseAuth mAuth;
    EditText txtNombreRegistro, txtNumeroTelefono,txtCedula,txtCorreoRegistro,txtPasswordRegistro, txtDireccion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        txtNombreRegistro = (EditText) findViewById(R.id.txtNombreRegistro);
        txtNumeroTelefono = (EditText) findViewById(R.id.txtNumeroTelefono);
        txtCedula = (EditText) findViewById(R.id.txtCedula);
        txtCorreoRegistro = (EditText) findViewById(R.id.txtCorreoRegistro);
        txtPasswordRegistro = (EditText) findViewById(R.id.txtPasswordRegistro);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        btnRegistrar = (Button) findViewById(R.id.btnRegistroUsuario);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");

        mAuth = FirebaseAuth.getInstance();

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtCorreoRegistro.getText().toString();
                String password = txtPasswordRegistro.getText().toString();

                mAuth.createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(RegistroActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Usuario usuario = new Usuario();
                            usuario.setNombre(txtNombreRegistro.getText().toString());
                            usuario.setCedula(txtCedula.getText().toString());
                            usuario.setDireccion(txtDireccion.getText().toString());
                            usuario.setTelefono(txtNumeroTelefono.getText().toString());
                            usuario.setCorreo(txtCorreoRegistro.getText().toString());
                            myRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario);
                            Toast.makeText(getApplicationContext(), "Registro Exitoso", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            Toast.makeText(getApplicationContext(), "No se pudo completar el registro", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }

}
