package com.cuartob.digitalsale.Activitys;

public class MenuCategoria {

    private String itemCategoria;

    public String getItemCategoria() {
        return itemCategoria;
    }

    public void setItemCategoria(String itemCategoria) {
        this.itemCategoria = itemCategoria;
    }

    public MenuCategoria(String itemCategoria) {
        this.itemCategoria = itemCategoria;
    }


}
